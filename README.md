# Web Services blocker controller

This go controller takes care of `blocking` and `unblocking` projects on [OKD4 PaaS](https://paas.docs.cern.ch).
The controller detects namespaces that need to be blocked or unblocked through annotations.
At the moment, these annotations are manually set by OKD admins.

```sh
# block project
oc label namespace $PROJECT okd.cern.ch/project-blocked=true --overwrite
# the controller sets the "project-blocker.okd.cern.ch/status" annotation

# unblock project
oc label namespace $PROJECT okd.cern.ch/project-blocked=false --overwrite
# the controller sets the "project-blocker.okd.cern.ch/status" annotation

# after successful unblocking, remove label entirely
oc label namespace $PROJECT okd.cern.ch/project-blocked-
```

Technically, blocking a site consists of:
1. Scaling down all workloads (Deployments, DeploymentConfigs, Statefulsets) in the project.
2. Changing the project pod quota to `0`. This makes it impossible to run new pods.
3. Removing all standalone pods, which takes care of pods which are not part of a workload abstraction (e.g. debug pods). Note that unlike the previous actions, this action is not reversible.

On the other hand, unblocking consists of:

1. Add previously-excludes routes back to the routers.
2. Reverting back the pod quota to the previous value.
3. Scaling up workloads to the previous replica number.

Note that the controller only gets triggered when transitioning from `okd.cern.ch/project-blocked=true` to `false` (and vice-versa).
It does not continually enforce the state (e.g. if an admin relaxes the pod quota without removing the namespace-blocking annotation, the controller won't set it back to 0).
This avoids a conflict with the operators on the App-Catalogue cluster (they are "dumb" Helm operators and don't take the blocked project label into consideration).

### Deployment

The controller is deployed on OKD4 clusters with the `PaaS` flavor, for details see [okd4-install docs](FIXME).

The installation (Deployment, ServiceAccount, Role etc.) is handled with a Helm chart in `./chart/`.
It creates a Kubernetes Deployment and a ServiceAccount.

```sh
helm install webservices-blocker-controller chart/
```

### Continuous Integration

Gitlab CI automatically runs [unit tests](./controller_test.go) for each commit.
These unit tests ensure the implementation details work as expected (annotations are added correctly, edge cases are handled properly, all resources are scaled etc.).
A coverage report for the tests can be generated locally with: `go test -coverprofile=cover.out . && go tool cover -html=cover.out -o coverage.html`

Afterwards, it builds a container image for the controller and pushes it to `gitlab-registry.cern.ch/paas-tools/paas-infra/webservices-blocker-controller`.

* On feature branches, the branch name is used as the image tag.
* On the master branch, a immutable tag is used, e.g. `RELEASE-DATE...`.

Additional integration tests are run as part of the [`PaaS` use-case tests in okd4-install](FIXME).
The integration ensure the Helm chart works properly and have some basic tests to make sure the controller does it's main job: blocking and unblocking projects (but they don't cover implementation details.)

### Local debugging

For development purposes, the controller can be run locally and attached to a dev cluster.

First, make sure the regular blocker-controller deployment (part of every PaaS cluster) is not running - otherwise there will be race-conditions between the two instances (*trust me, been there, did that*):

```
oc -n openshift-cern-argocd delete application/webservices-blocker-controller
oc get pods -A | grep blocker-controller
```

Then, you can run the controller locally with the following command.
A kubeconfig file with `cluster-admin` permissions and the Go toolchain is required.

```
go run . -kubeconfig /path/to/kubeconfig -v 5
```
