/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	"strconv"
	"time"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	coreinformers "k8s.io/client-go/informers/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	corelisters "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
	"k8s.io/klog/v2"

	ocapps "github.com/openshift/client-go/apps/clientset/versioned"
	// routev1 "github.com/openshift/client-go/route/clientset/versioned/typed/route/v1"
)

const controllerAgentName = "webservices-blocker-controller"

// Constants for this controller
const (
	blockedProjectLabel         = "okd.cern.ch/project-blocked"
	blockedStatusAnnotation     = "project-blocker.okd.cern.ch/status"
	blockedScaledDownAnnotation = "project-blocker.okd.cern.ch/old-pod-replica-number"
	blockedPodQuotaAnnotation   = "project-blocker.okd.cern.ch/old-pod-quota"
	blockedCronJobAnnotation    = "project-blocker.okd.cern.ch/old-suspend-status"
)

const (
	// SuccessSynced is used as part of the Event 'reason' when a Foo is synced
	SuccessBlocked   = "Blocked"
	SuccessUnblocked = "Unblocked"
	FailedSynced     = "Failed"
	InvalidLabels    = "InvalidLabels"
	// ErrResourceExists is used as part of the Event 'reason' when a Foo fails
	// to sync due to a Deployment of the same name already existing.
	ErrResourceExists = "ErrResourceExists"

	// MessageResourceSynced is the message used for an Event fired when a Foo
	// is synced successfully
	MessageStatusBlocked   = "Project blocking completed"
	MessageStatusUnblocked = "Project unblocking completed"
)

// Controller is the controller implementation for Foo resources
type Controller struct {
	// kubeclientset is a standard kubernetes clientset
	kubeclientset kubernetes.Interface
	// client for accessing apps.openshift.io/v1
	ocAppsClient ocapps.Interface

	namespacesLister corelisters.NamespaceLister
	namespacesSynced cache.InformerSynced

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// NewController returns a new controller
func NewController(
	kubeclientset kubernetes.Interface,
	ocAppsClient ocapps.Interface,
	namespaceInformer coreinformers.NamespaceInformer,
) *Controller {

	// Create event broadcaster
	klog.V(4).Info("Creating event broadcaster")
	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartStructuredLogging(0)
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: kubeclientset.CoreV1().Events("")})
	recorder := eventBroadcaster.NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	controller := &Controller{
		kubeclientset:    kubeclientset,
		ocAppsClient:     ocAppsClient,
		namespacesLister: namespaceInformer.Lister(),
		namespacesSynced: namespaceInformer.Informer().HasSynced,
		workqueue:        workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "Namespaces"),
		recorder:         recorder,
	}

	// Set up an event handler for when Namespace resources change. This
	// handler will lookup the owner of the given Deployment, and if it is
	// owned by a Foo resource then the handler will enqueue that Foo resource for
	// processing. This way, we don't need to implement custom logic for
	// handling Deployment resources. More info on this pattern:
	// https://github.com/kubernetes/community/blob/8cafef897a22026d42f5e5bb3f104febe7e29830/contributors/devel/controllers.md
	namespaceInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: controller.handleObject,
		UpdateFunc: func(old, new interface{}) {
			newNs := new.(*corev1.Namespace)
			oldNs := old.(*corev1.Namespace)
			if newNs.ResourceVersion == oldNs.ResourceVersion {
				// Periodic resync will send update events for all known namespaces.
				// Two different versions of the same namespace will always have different RVs.
				return
			}
			controller.handleObject(new)
		},
		DeleteFunc: controller.handleObject,
	})

	return controller
}

// Run will set up the event handlers for types we are interested in, as well
// as syncing informer caches and starting workers. It will block until stopCh
// is closed, at which point it will shutdown the workqueue and wait for
// workers to finish processing their current work items.
func (c *Controller) Run(workers int, stopCh <-chan struct{}) error {
	defer utilruntime.HandleCrash()
	defer c.workqueue.ShutDown()

	// Start the informer factories to begin populating the informer caches
	klog.Infof("Starting %s\n", controllerAgentName)

	// Wait for the caches to be synced before starting workers
	klog.Info("Waiting for informer caches to sync")
	if ok := cache.WaitForCacheSync(stopCh, c.namespacesSynced); !ok {
		return fmt.Errorf("failed to wait for caches to sync")
	}

	klog.Info("Starting workers")
	// Launch the desired number of workers to process namespace resources
	for i := 0; i < workers; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	klog.Info("Started workers")
	<-stopCh
	klog.Info("Shutting down workers")

	return nil
}

// runWorker is a long-running function that will continually call the
// processNextWorkItem function in order to read and process a message on the
// workqueue.
func (c *Controller) runWorker() {
	for c.processNextWorkItem() {
	}
}

// processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			utilruntime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}
		// Run the syncHandler, passing it the namespace/name string of the resource to be synced.
		klog.V(4).Infof("Running syncHandler for key '%s'...\n", key)
		if err := c.syncHandler(key); err != nil {
			// Put the item back on the workqueue to handle any transient errors.
			c.workqueue.AddRateLimited(key)
			return fmt.Errorf("error syncing '%s': %s, requeuing", key, err.Error())
		}
		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		klog.Infof("Successfully synced '%s'", key)
		return nil
	}(obj)

	if err != nil {
		utilruntime.HandleError(err)
		return true
	}

	return true
}

// syncHandler compares the actual state with the desired, and attempts to
// converge the two. It then updates the Status block of the Foo resource
// with the current status of the resource.
// If an error occurs, the syncHandler will be called again at a later point in time.
func (c *Controller) syncHandler(key string) error {
	// Convert the namespace/name string into a distinct namespace and name
	_, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		utilruntime.HandleError(fmt.Errorf("invalid resource key: %s", key))
		return nil
	}

	ns, err := c.namespacesLister.Get(name)
	if err != nil {
		// The namespace may no longer exist, in which case we stop processing.
		if errors.IsNotFound(err) {
			utilruntime.HandleError(fmt.Errorf("namespace '%s' in work queue no longer exists", key))
			return nil
		}

		return err
	}

	ctx := context.Background()

	value, exists := ns.Labels[blockedProjectLabel]
	if exists {
		if value == "true" {
			klog.Infof("Blocking namespace '%s'\n", ns.GetName())

			if err := c.block(ctx, ns); err != nil {
				c.eventForNamespace(ns, corev1.EventTypeWarning, FailedSynced, err.Error())
				return err
			}
			c.eventForNamespace(ns, corev1.EventTypeNormal, SuccessBlocked, MessageStatusBlocked)
		} else if value == "false" {
			klog.Infof("Unblocking namespace '%s'\n", ns.GetName())

			if err := c.unblock(ctx, ns); err != nil {
				c.eventForNamespace(ns, corev1.EventTypeWarning, FailedSynced, err.Error())
				return err
			}
			c.eventForNamespace(ns, corev1.EventTypeNormal, SuccessUnblocked, MessageStatusUnblocked)
		} else {
			// unsupported value
			c.eventForNamespace(ns, corev1.EventTypeWarning, InvalidLabels, "Blocking label has unsupported value: '"+value+"'; possible values are 'true' and 'false'")
			return nil
		}
	} else {
		// label not present on namespace, do nothing
		klog.V(4).Infof("Ignoring namespace %s because it does not have the %s label.\n", ns.GetName(), blockedProjectLabel)
	}

	return nil
}

func (c *Controller) eventForNamespace(ns *corev1.Namespace, eventtype, reason, message string) {
	// do not modify the original object
	namespace := ns.DeepCopy()
	// this is a workaround so the Events get recorded in the correct namespace (instead of "default" namespace)
	// ref. https://github.com/kubernetes/client-go/blob/v0.26.1/tools/record/event.go#L376
	namespace.Namespace = namespace.Name

	c.recorder.Event(namespace, eventtype, reason, message)
}

// handleObject will take any resource implementing metav1.Object and attempt
// to find the Foo resource that 'owns' it. It does this by looking at the
// objects metadata.ownerReferences field for an appropriate OwnerReference.
// It then enqueues that Foo resource to be processed. If the object does not
// have an appropriate OwnerReference, it will simply be skipped.
func (c *Controller) handleObject(obj interface{}) {
	var object metav1.Object
	var ok bool
	if object, ok = obj.(metav1.Object); !ok {
		tombstone, ok := obj.(cache.DeletedFinalStateUnknown)
		if !ok {
			utilruntime.HandleError(fmt.Errorf("error decoding object, invalid type"))
			return
		}
		object, ok = tombstone.Obj.(metav1.Object)
		if !ok {
			utilruntime.HandleError(fmt.Errorf("error decoding object tombstone, invalid type"))
			return
		}
		klog.V(4).Infof("Recovered deleted object '%s' from tombstone", object.GetName())
	}
	klog.V(4).Infof("Processing object: %s", object.GetName())

	// convert the object into a namespace/name string which is then put onto the work queue.
	var key string
	var err error
	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		utilruntime.HandleError(err)
		return
	}
	c.workqueue.Add(key)
}

func int32Ptr(i int32) *int32 { return &i }

// getAnnotations always returns a valid annotations map,
// never nil like the default kubernetes client-go
func getAnnotations(o metav1.ObjectMeta) map[string]string {
	a := o.GetAnnotations()
	if a == nil {
		a = map[string]string{}
	}
	return a
}

func boolPtr(value bool) *bool {
    b := value
    return &b
}

// Blocking consists stopping all pods and not allowing the user to create them back. To do so:
// 1. Set the project quota max number of pods to 0. This will prevent new pods to be created (either manually or from another controller)
// 2. All the scalable objects are scaled to 0. This will stop all permanently running pods.
// 3. Wait 10 seconds and delete all pods (if there are any left). This will ensure all pods are gone
func (c *Controller) block(ctx context.Context, ns *corev1.Namespace) error {
	// Scale down all scalable resources (Deployments, DeploymentConfigs, StatefulSets)
	klog.V(4).Infof("Scaling down Deployments (if any) in namespace '%s'\n", ns.GetName())
	deploymentClient := c.kubeclientset.AppsV1().Deployments(ns.GetName())
	deployments, err := deploymentClient.List(ctx, metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, deployment := range deployments.Items {
		if *deployment.Spec.Replicas > 0 {
			klog.Infof("Proceeding to scale down deployment %s/%s ...\n", deployment.GetNamespace(), deployment.GetName())
			annotations := getAnnotations(deployment.ObjectMeta)
			annotations[blockedScaledDownAnnotation] = fmt.Sprint(*deployment.Spec.Replicas)
			deployment.SetAnnotations(annotations)
			deployment.Spec.Replicas = int32Ptr(0)
			_, err := deploymentClient.Update(ctx, &deployment, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
			klog.Infof("Deployment %s/%s successfully scaled down\n", deployment.GetNamespace(), deployment.GetName())
		}
	}

	klog.V(4).Infof("Scaling down DeploymentConfigs (if any) in namespace '%s'\n", ns.GetName())
	dcClient := c.ocAppsClient.AppsV1().DeploymentConfigs(ns.GetName())
	deploymentConfigs, err := dcClient.List(ctx, metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, dc := range deploymentConfigs.Items {
		if dc.Spec.Replicas > 0 {
			klog.Infof("Proceeding to scale down DeploymentConfig %s/%s ...\n", dc.GetNamespace(), dc.GetName())
			annotations := getAnnotations(dc.ObjectMeta)
			annotations[blockedScaledDownAnnotation] = fmt.Sprint(dc.Spec.Replicas)
			dc.SetAnnotations(annotations)
			dc.Spec.Replicas = 0
			_, err := dcClient.Update(ctx, &dc, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
			klog.Infof("DeploymentConfig %s/%s successfully scaled down\n", dc.GetNamespace(), dc.GetName())
		}
	}

	klog.V(4).Infof("Scaling down StatefulSets (if any) in namespace '%s'\n", ns.GetName())
	stsClient := c.kubeclientset.AppsV1().StatefulSets(ns.GetName())
	statefulsets, err := stsClient.List(ctx, metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, sts := range statefulsets.Items {
		if *sts.Spec.Replicas > 0 {
			klog.Infof("Proceeding to scale down StatefulSet %s/%s ...\n", sts.GetNamespace(), sts.GetName())
			annotations := getAnnotations(sts.ObjectMeta)
			annotations[blockedScaledDownAnnotation] = fmt.Sprint(*sts.Spec.Replicas)
			sts.SetAnnotations(annotations)
			sts.Spec.Replicas = int32Ptr(0)
			_, err := stsClient.Update(ctx, &sts, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
			klog.Infof("StatefulSet %s/%s successfully scaled down\n", sts.GetNamespace(), sts.GetName())
		}
	}

	// Suspend the CronJobs
	klog.V(4).Infof("Suspending CronJobs (if any) in namespace '%s'\n", ns.GetName())
	cjClient := c.kubeclientset.BatchV1().CronJobs(ns.GetName())
	cronjobs, err := cjClient.List(ctx, metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, cj := range cronjobs.Items {
		if cj.Spec.Suspend == nil || *cj.Spec.Suspend == false {
			klog.Infof("Proceeding to suspend CronJob %s/%s ...\n", cj.GetNamespace(), cj.GetName())
			annotations := getAnnotations(cj.ObjectMeta)
			annotations[blockedCronJobAnnotation] = "false"
			cj.SetAnnotations(annotations)
			cj.Spec.Suspend = boolPtr(true)
			_, err := cjClient.Update(ctx, &cj, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
			klog.Infof("Cronjob %s/%s successfully suspended\n", cj.GetNamespace(), cj.GetName())
		}
	}

	// Set project pod quota to 0 to prevent creation of new pods
	resourceQuotaClient := c.kubeclientset.CoreV1().ResourceQuotas(ns.GetName())
	quota, err := resourceQuotaClient.Get(ctx, ns.GetName(), metav1.GetOptions{})
	if err != nil {
		return err
	}
	quotaAnnotations := getAnnotations(quota.ObjectMeta)

	// Only proceed if the quota has not already been blocked
	if _, exists := quotaAnnotations[blockedPodQuotaAnnotation]; !exists {
		// Store the old quota as an annotation (so we can revert it)
		quotaAnnotations[blockedPodQuotaAnnotation] = quota.Spec.Hard.Pods().String()
		quota.SetAnnotations(quotaAnnotations)
		if len(quota.Spec.Hard) == 0 {
			quota.Spec.Hard = make(corev1.ResourceList)
		}
		// Change pod quota to 0, so no new pods can be created anymore
		quota.Spec.Hard[corev1.ResourcePods] = resource.MustParse("0")

		// Save changes to Resource quota
		_, err = resourceQuotaClient.Update(ctx, quota, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
		klog.Infof("Pod quota in namespace '%s' successfully set to 0\n", ns.GetName())
	} else {
		klog.Infof("Pod quota is already blocked, skipping quota update\n")
	}

	// After grace-period, delete the remaining pods (created manually or by other controllers such a Jobs etc.)
	podClient := c.kubeclientset.CoreV1().Pods(ns.GetName())
	pods, err := podClient.List(ctx, metav1.ListOptions{})
	if err != nil {
		return err
	}
	if len(pods.Items) > 0 {
		time.Sleep(10 * time.Second)
		// need to re-run query because the state might have changed since before the sleep
		pods, err = podClient.List(ctx, metav1.ListOptions{})
		if err != nil {
			return err
		}
		// https://kubernetes.io/docs/concepts/overview/working-with-objects/owners-dependents/
		for _, pod := range pods.Items {
			klog.Infof("Deleting pod %s/%s ...\n", pod.GetNamespace(), pod.GetName())
			err := podClient.Delete(ctx, pod.GetName(), metav1.DeleteOptions{})
			if err != nil {
				// Do not abort if pod fails to be deleted
				klog.Errorln(err)
				continue
			}
			klog.Infof("Pod %s/%s successfully deleted\n", pod.GetNamespace(), pod.GetName())
		}
	}

	// Update namespace metadata
	klog.V(4).Infof("Marking namespace '%s' as blocked\n")
	nsClient := c.kubeclientset.CoreV1().Namespaces()
	ns.Annotations[blockedStatusAnnotation] = MessageStatusBlocked
	_, err = nsClient.Update(ctx, ns, metav1.UpdateOptions{})
	if err != nil {
		return err
	}

	return nil
}

// Unblocking the project consists of the following steps:
// 1. Restore original project pod quota
// 2. Scale up Deployments, DeploymentConfigs and StatefulSets to their original replicas,
//    as indicated by the associated annotation
// NOTE: standalone pods which have been deleted in the blocking process will not be restored
func (c *Controller) unblock(ctx context.Context, ns *corev1.Namespace) error {
	// Set project pod quota to 0 to prevent creation of new pods
	resourceQuotaClient := c.kubeclientset.CoreV1().ResourceQuotas(ns.GetName())
	quota, err := resourceQuotaClient.Get(ctx, ns.GetName(), metav1.GetOptions{})
	if err != nil {
		return err
	}

	// Extract original pod quota from annotation and remove it
	quotaAnnotations := getAnnotations(quota.ObjectMeta)
	podQuotaString, exists := quotaAnnotations[blockedPodQuotaAnnotation]
	if !exists {
		klog.Infof("Project quota '%s' is missing the '%s' annotation. Assuming it has already been unblocked\n", ns.GetName(), blockedPodQuotaAnnotation)
	} else {
		delete(quotaAnnotations, blockedPodQuotaAnnotation)
		quota.SetAnnotations(quotaAnnotations)

		// Parse pod quota from string to quantity type
		podQuota, err := resource.ParseQuantity(podQuotaString)
		if err != nil {
			return fmt.Errorf("Failed to parse '%s' annotation of quota '%s'. Cannot restore original pod quota\n", blockedPodQuotaAnnotation, ns.GetName())
		}
		// Set the original pod quota
		quota.Spec.Hard[corev1.ResourcePods] = podQuota

		// Save changes to Resource quota
		_, err = resourceQuotaClient.Update(ctx, quota, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
		klog.Infof("Pod quota in namespace '%s' successfully set to 0\n", ns.GetName())
	}

	// Find all deployments in namespace and scale them back up
	// if they were scaled down by the controller
	klog.V(4).Infof("Scaling up Deployments in namespace '%s'\n", ns.GetName())
	deploymentClient := c.kubeclientset.AppsV1().Deployments(ns.GetName())
	deployments, err := deploymentClient.List(ctx, metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, deployment := range deployments.Items {
		deploymentAnnotations := getAnnotations(deployment.ObjectMeta)
		if replicaStr, ok := deploymentAnnotations[blockedScaledDownAnnotation]; ok {
			replicas, err := strconv.ParseInt(replicaStr, 10, 32)
			if err != nil {
				return fmt.Errorf("The replica count stored in annotations '%s' on Deployment %s is malformed. Cannot scale up.",
					blockedScaledDownAnnotation, deployment.GetName())
			}
			klog.Infof("Proceeding to scale up deployment %s/%s to %d replicas...\n", deployment.GetNamespace(), deployment.GetName(), replicas)
			deployment.Spec.Replicas = int32Ptr(int32(replicas))
			delete(deploymentAnnotations, blockedScaledDownAnnotation)
			deployment.SetAnnotations(deploymentAnnotations)
			_, err = deploymentClient.Update(ctx, &deployment, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
			klog.Infof("Deployment %s/%s successfully scaled down\n", deployment.GetNamespace(), deployment.GetName())
		} else {
			klog.Infof("Deployment %s/%s does not have '%s' annotation, ignoring...\n", deployment.GetNamespace(), deployment.GetName(), blockedScaledDownAnnotation)
		}
	}

	// do the same for statefulsets
	klog.V(4).Infof("Scaling up Deployments in namespace '%s'\n", ns.GetName())
	stsClient := c.kubeclientset.AppsV1().StatefulSets(ns.GetName())
	statefulsets, err := stsClient.List(ctx, metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, sts := range statefulsets.Items {
		deploymentAnnotations := getAnnotations(sts.ObjectMeta)
		if replicaStr, ok := deploymentAnnotations[blockedScaledDownAnnotation]; ok {
			replicas, err := strconv.ParseInt(replicaStr, 10, 32)
			if err != nil {
				return fmt.Errorf("The replica count stored in annotations '%s' on StatefulSet %s is malformed. Cannot scale up.",
					blockedScaledDownAnnotation, sts.GetName())
			}
			klog.Infof("Proceeding to scale up StatefulSet %s/%s to %d replicas...\n", sts.GetNamespace(), sts.GetName(), replicas)
			sts.Spec.Replicas = int32Ptr(int32(replicas))
			delete(deploymentAnnotations, blockedScaledDownAnnotation)
			sts.SetAnnotations(deploymentAnnotations)
			_, err = stsClient.Update(ctx, &sts, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
			klog.Infof("StatefulSet %s/%s successfully scaled up\n", sts.GetNamespace(), sts.GetName())
		} else {
			klog.Infof("StatefulSet %s/%s does not have '%s' annotation, ignoring...\n", sts.GetNamespace(), sts.GetName(), blockedScaledDownAnnotation)
		}
	}

	// do the same for DeploymentConfigs
	klog.V(4).Infof("Scaling up DeploymentConfigs in namespace '%s'\n", ns.GetName())
	dcClient := c.ocAppsClient.AppsV1().DeploymentConfigs(ns.GetName())
	deploymentConfigs, err := dcClient.List(ctx, metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, dc := range deploymentConfigs.Items {
		dcAnnotations := getAnnotations(dc.ObjectMeta)
		if replicaStr, ok := dcAnnotations[blockedScaledDownAnnotation]; ok {
			replicas, err := strconv.ParseInt(replicaStr, 10, 32)
			if err != nil {
				return fmt.Errorf("The replica count stored in annotations '%s' on DeploymentConfig %s is malformed. Cannot scale up.",
					blockedScaledDownAnnotation, dc.GetName())
			}
			klog.Infof("Proceeding to scale up DeploymentConfig %s/%s to %d replicas...\n", dc.GetNamespace(), dc.GetName(), replicas)
			delete(dcAnnotations, blockedScaledDownAnnotation)
			dc.SetAnnotations(dcAnnotations)
			dc.Spec.Replicas = int32(replicas)
			_, err = dcClient.Update(ctx, &dc, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
			klog.Infof("DeploymentConfig %s/%s successfully scaled up\n", dc.GetNamespace(), dc.GetName())
		} else {
			klog.Infof("DeploymentConfig %s/%s does not have '%s' annotation, ignoring...\n", dc.GetNamespace(), dc.GetName(), blockedScaledDownAnnotation)
		}
	}

	// do the same for CronJobs
	klog.V(4).Infof("Resuming Cronjobs in namespace '%s'\n", ns.GetName())
	cjClient := c.kubeclientset.BatchV1().CronJobs(ns.GetName())
	cronjobs, err := cjClient.List(ctx, metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, cj := range cronjobs.Items {
		cjAnnotations := getAnnotations(cj.ObjectMeta)
		if suspendStr, ok := cjAnnotations[blockedCronJobAnnotation]; ok {
			suspend, err := strconv.ParseBool(suspendStr)
			if err != nil {
				return fmt.Errorf("The old suspend state stored in annotations '%s' on CronJob %s is malformed. Cannot scale up.",
					blockedCronJobAnnotation, cj.GetName())
			}
			klog.Infof("Proceeding to resume Cronjob %s/%s ...\n", cj.GetNamespace(), cj.GetName())
			delete(cjAnnotations, blockedCronJobAnnotation)
			cj.SetAnnotations(cjAnnotations)
			*cj.Spec.Suspend = suspend
			_, err = cjClient.Update(ctx, &cj, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
			klog.Infof("CronJob %s/%s successfully resumed\n", cj.GetNamespace(), cj.GetName())
		} else {
			klog.Infof("Cronjob %s/%s does not have '%s' annotation, ignoring...\n", cj.GetNamespace(), cj.GetName(), blockedCronJobAnnotation)
		}
	}




	// Update namespace metadata
	klog.V(4).Infof("Marking namespace '%s' as unblocked\n")
	nsClient := c.kubeclientset.CoreV1().Namespaces()
	ns.Annotations[blockedStatusAnnotation] = MessageStatusUnblocked
	_, err = nsClient.Update(ctx, ns, metav1.UpdateOptions{})
	if err != nil {
		return err
	}

	return nil
}
