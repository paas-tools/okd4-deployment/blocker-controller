/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"reflect"
	"testing"
	"time"

	ocappsv1 "github.com/openshift/api/apps/v1"
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/diff"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"

	// "fake" clients for testing
	ocappsfake "github.com/openshift/client-go/apps/clientset/versioned/fake"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	core "k8s.io/client-go/testing"
)

var (
	alwaysReady        = func() bool { return true }
	noResyncPeriodFunc = func() time.Duration { return 0 }
)

type fixture struct {
	t *testing.T

	kubeClient   *k8sfake.Clientset
	ocAppsClient *ocappsfake.Clientset
	// Objects to put in the store.
	namespaceLister []*corev1.Namespace
	// deploymentLister []*apps.Deployment
	// Actions expected to happen on the client.
	kubeactions []core.Action
	// Objects from here preloaded into NewSimpleFake.
	kubeobjects []runtime.Object
	ocobjects   []runtime.Object
}

func newFixture(t *testing.T) *fixture {
	f := &fixture{}
	f.t = t
	f.kubeobjects = []runtime.Object{}
	f.ocobjects = []runtime.Object{}
	return f
}

func newNamespace(name string) *corev1.Namespace {
	return &corev1.Namespace{
		TypeMeta: metav1.TypeMeta{APIVersion: corev1.SchemeGroupVersion.String()},
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Annotations: map[string]string{},
			Labels:      map[string]string{},
		},
	}
}

func newDeployment(name string, namespace string, replicas *int32) *appsv1.Deployment {
	return &appsv1.Deployment{
		TypeMeta: metav1.TypeMeta{APIVersion: appsv1.SchemeGroupVersion.String()},
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Namespace:   namespace,
			Annotations: map[string]string{},
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: replicas,
		},
	}
}

func newDeploymentConfig(name string, namespace string, replicas int32) *ocappsv1.DeploymentConfig {
	return &ocappsv1.DeploymentConfig{
		TypeMeta: metav1.TypeMeta{APIVersion: ocappsv1.SchemeGroupVersion.String()},
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Namespace:   namespace,
			Annotations: map[string]string{},
		},
		Spec: ocappsv1.DeploymentConfigSpec{
			Replicas: replicas,
		},
	}
}

func newCronjob(name string, namespace string) *batchv1.CronJob {
	return &batchv1.CronJob{
		TypeMeta: metav1.TypeMeta{APIVersion: batchv1.SchemeGroupVersion.String()},
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Namespace:   namespace,
			Annotations: map[string]string{},
		},
		Spec: batchv1.CronJobSpec{
			Schedule: "0 0 * * *",
		},
	}
}


func newPod(name string, namespace string) *corev1.Pod {
	return &corev1.Pod{
		TypeMeta: metav1.TypeMeta{APIVersion: corev1.SchemeGroupVersion.String()},
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Namespace:   namespace,
			Annotations: map[string]string{},
		},
		Spec: corev1.PodSpec{},
	}
}

func newQuota(name string, namespace string) *corev1.ResourceQuota {
	return &corev1.ResourceQuota{
		TypeMeta: metav1.TypeMeta{APIVersion: appsv1.SchemeGroupVersion.String()},
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Namespace:   namespace,
			Annotations: map[string]string{},
		},
		Spec: corev1.ResourceQuotaSpec{
			Hard: corev1.ResourceList{
				corev1.ResourcePods: resource.MustParse("42"),
			},
		},
	}
}

func (f *fixture) newController() (*Controller, kubeinformers.SharedInformerFactory) {
	// TODO: could automatically differentiate between k8s and oc objects - maybe?
	f.kubeClient = k8sfake.NewSimpleClientset(f.kubeobjects...)
	f.ocAppsClient = ocappsfake.NewSimpleClientset(f.ocobjects...)

	i := kubeinformers.NewSharedInformerFactory(f.kubeClient, noResyncPeriodFunc())

	c := NewController(f.kubeClient, f.ocAppsClient, i.Core().V1().Namespaces())

	c.namespacesSynced = alwaysReady
	c.recorder = &record.FakeRecorder{}

	for _, n := range f.namespaceLister {
		i.Core().V1().Namespaces().Informer().GetIndexer().Add(n)
	}

	// for _, d := range f.deploymentLister {
	// 	i.Apps().V1().Deployments().Informer().GetIndexer().Add(d)
	// }

	return c, i
}

func (f *fixture) run(fooName string) {
	f.runController(fooName, true, false)
}

func (f *fixture) runExpectError(fooName string) {
	f.runController(fooName, true, true)
}

func (f *fixture) runController(fooName string, startInformers bool, expectError bool) {
	c, i := f.newController()
	if startInformers {
		stopCh := make(chan struct{})
		defer close(stopCh)
		i.Start(stopCh)
	}

	err := c.syncHandler(fooName)
	if !expectError && err != nil {
		f.t.Errorf("error syncing: %v", err)
	} else if expectError && err == nil {
		f.t.Error("expected error syncing, got nil")
	}

	k8sActions := filterInformerActions(f.kubeClient.Actions(), f.ocAppsClient.Actions())
	for i, action := range k8sActions {
		if len(f.kubeactions) < i+1 {
			f.t.Errorf("%d unexpected actions: %+v", len(k8sActions)-len(f.kubeactions), k8sActions[i:])
			break
		}

		expectedAction := f.kubeactions[i]
		checkAction(expectedAction, action, f.t)
	}

	if len(f.kubeactions) > len(k8sActions) {
		f.t.Errorf("%d additional expected actions:%+v", len(f.kubeactions)-len(k8sActions), f.kubeactions[len(k8sActions):])
	}
}

// checkAction verifies that expected and actual actions are equal and both have
// same attached resources
func checkAction(expected, actual core.Action, t *testing.T) {
	if !(expected.Matches(actual.GetVerb(), actual.GetResource().Resource) && actual.GetSubresource() == expected.GetSubresource()) {
		t.Errorf("Expected\n\t%#v\ngot\n\t%#v", expected, actual)
		return
	}

	if reflect.TypeOf(actual) != reflect.TypeOf(expected) {
		t.Errorf("Action has wrong type. Expected: %t. Got: %t", expected, actual)
		return
	}

	switch a := actual.(type) {
	case core.CreateActionImpl:
		e, _ := expected.(core.CreateActionImpl)
		expObject := e.GetObject()
		object := a.GetObject()

		if !reflect.DeepEqual(expObject, object) {
			t.Errorf("Action %s %s has wrong object\nDiff:\n %s",
				a.GetVerb(), a.GetResource().Resource, diff.ObjectGoPrintSideBySide(expObject, object))
		}
	case core.UpdateActionImpl:
		e, _ := expected.(core.UpdateActionImpl)
		expObject := e.GetObject()
		object := a.GetObject()

		if !reflect.DeepEqual(expObject, object) {
			t.Errorf("Action %s %s has wrong object\nDiff:\n %s",
				a.GetVerb(), a.GetResource().Resource, diff.ObjectGoPrintSideBySide(expObject, object))
			// a.GetVerb(), a.GetResource().Resource, diff.ObjectDiff(expObject, object))
		}
	case core.PatchActionImpl:
		e, _ := expected.(core.PatchActionImpl)
		expPatch := e.GetPatch()
		patch := a.GetPatch()

		if !reflect.DeepEqual(expPatch, patch) {
			t.Errorf("Action %s %s has wrong patch\nDiff:\n %s",
				a.GetVerb(), a.GetResource().Resource, diff.ObjectGoPrintSideBySide(expPatch, patch))
		}
	case core.DeleteActionImpl:
		e, _ := expected.(core.DeleteActionImpl)
		expName := e.GetName()
		name := a.GetName()

		if expName != name {
			t.Errorf("Action %s %s has wrong name: expected '%s', got '%s'",
				a.GetVerb(), a.GetResource().Resource, expName, name)
		}
	default:
		t.Errorf("Uncaptured Action '%s' %s/%s %s, you should explicitly add a case to capture it",
			actual.GetVerb(), actual.GetResource().Group, actual.GetResource().Version, actual.GetResource().Resource)
	}
}

// filterInformerActions filters list and watch actions for testing resources.
// Since list and watch don't change resource state we can filter it to lower
// noise level in our tests.
func filterInformerActions(actions ...[]core.Action) []core.Action {
	ret := []core.Action{}
	for _, actionList := range actions {
		for _, action := range actionList {
			if action.GetVerb() == "get" ||
				action.GetVerb() == "list" ||
				action.GetVerb() == "watch" {
				continue
			}
			ret = append(ret, action)
		}
	}

	return ret
}

// Note: we are not checking the "Group" and "Version" fields because we are also not checking them in checkAction()
func (f *fixture) expectGetAction(resource corev1.ResourceName, namespace string, name string) {
	f.kubeactions = append(f.kubeactions, core.NewGetAction(schema.GroupVersionResource{Version: "v1", Resource: fmt.Sprint(resource)}, namespace, name))
}

func (f *fixture) expectUpdateDeploymentAction(o *appsv1.Deployment) {
	f.kubeactions = append(f.kubeactions, core.NewUpdateAction(schema.GroupVersionResource{Resource: "deployments"}, o.Namespace, o))
}

func (f *fixture) expectUpdateAction(o runtime.Object) {
	var gvr, ns string
	switch (o).(type) {
	case *corev1.ResourceQuota:
		gvr = fmt.Sprint(corev1.ResourceQuotas)
		ns = (o).(*corev1.ResourceQuota).GetNamespace()
	case *corev1.Namespace:
		gvr = "namespaces"
		ns = ""
	case *appsv1.Deployment:
		gvr = "deployments"
		ns = (o).(*appsv1.Deployment).GetNamespace()
	case *ocappsv1.DeploymentConfig:
		gvr = "deploymentconfigs"
		ns = (o).(*ocappsv1.DeploymentConfig).GetNamespace()
	case *batchv1.CronJob:
		gvr = "cronjobs"
		ns = (o).(*batchv1.CronJob).GetNamespace()
	default:
		panic("Unknown type for expectUpdateAction")
	}
	f.kubeactions = append(f.kubeactions, core.NewUpdateAction(schema.GroupVersionResource{Resource: gvr}, ns, o))
}

func (f *fixture) expectDeleteAction(o runtime.Object) {
	var gvr, ns, name string
	switch (o).(type) {
	case *corev1.Pod:
		gvr = fmt.Sprint(corev1.ResourcePods)
		ns = (o).(*corev1.Pod).GetNamespace()
		name = (o).(*corev1.Pod).GetName()
	default:
		fmt.Print("Unknown type")
	}
	f.kubeactions = append(f.kubeactions, core.NewDeleteAction(schema.GroupVersionResource{Resource: gvr}, ns, name))
}

func getKey(n *corev1.Namespace, t *testing.T) string {
	key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(n)
	if err != nil {
		t.Errorf("Unexpected error getting key for foo %v: %v", n.Name, err)
		return ""
	}
	return key
}

func TestDoNothing(t *testing.T) {
	f := newFixture(t)
	ns := newNamespace(t.Name())
	deploy := newDeployment("test-deploy", ns.Name, int32Ptr(1))
	quota := newQuota(ns.Name, ns.Name)

	f.namespaceLister = append(f.namespaceLister, ns)
	f.kubeobjects = append(f.kubeobjects, deploy, ns, quota)

	// we don't expect any actions here, except the ones whitelisted by filterInformerActions()
	f.run(getKey(ns, t))
}

func TestBlockNamespaceWithoutQuota(t *testing.T) {
	f := newFixture(t)
	ns := newNamespace(t.Name())

	// block namespace
	ns.Labels["okd.cern.ch/project-blocked"] = "true"

	f.namespaceLister = append(f.namespaceLister, ns)
	f.kubeobjects = append(f.kubeobjects, ns)

	// we expect an error because the controller cannot find a quota for "test123"
	f.runExpectError(getKey(ns, t))
}

func TestBlockNamespaceScaleDeployment(t *testing.T) {
	f := newFixture(t)
	ns := newNamespace(t.Name())
	ns.Labels["okd.cern.ch/project-blocked"] = "true"
	deploy := newDeployment("test-deploy", ns.Name, int32Ptr(1))
	deploy.Annotations["random-annotation"] = "lorem-ipsum"
	quota := newQuota(ns.Name, ns.Name)
	quota.Annotations["another-annotation"] = "lorem-ipsum"

	// add the pre-created object into our fake environment
	f.namespaceLister = append(f.namespaceLister, ns)
	f.kubeobjects = append(f.kubeobjects, ns, deploy, quota)

	// deployment should be scaled to zero and annotation should be added
	expDeployment := deploy.DeepCopy()
	expDeployment.Spec.Replicas = int32Ptr(0)
	expDeployment.Annotations["project-blocker.okd.cern.ch/old-pod-replica-number"] = "1"
	f.expectUpdateAction(expDeployment)

	// quota should be set to zero and annotation should be added
	expQuota := quota.DeepCopy()
	expQuota.Spec.Hard[corev1.ResourcePods] = resource.MustParse("0")
	expQuota.ObjectMeta.Annotations["project-blocker.okd.cern.ch/old-pod-quota"] = "42"
	f.expectUpdateAction(expQuota)

	// namespace should be annotated
	expNs := ns.DeepCopy()
	expNs.ObjectMeta.Annotations["project-blocker.okd.cern.ch/status"] = "Project blocking completed"
	f.expectUpdateAction(expNs)

	// for _, a := range f.kubeactions {
	// 	fmt.Printf("action: %v\n", a)
	// }

	// run the controller to see if it works
	f.run(getKey(ns, t))
}

func TestBlockNamespaceScaleDeploymentConfig(t *testing.T) {
	f := newFixture(t)
	ns := newNamespace(t.Name())
	ns.Labels["okd.cern.ch/project-blocked"] = "true"
	dc := newDeploymentConfig("test-dc", ns.Name, 1)
	quota := newQuota(ns.Name, ns.Name)

	// add the pre-created object into our fake environment
	f.namespaceLister = append(f.namespaceLister, ns)
	f.kubeobjects = append(f.kubeobjects, ns, quota)
	f.ocobjects = append(f.ocobjects, dc)

	// quota should be set to zero and annotation should be added
	expQuota := quota.DeepCopy()
	expQuota.Spec.Hard[corev1.ResourcePods] = resource.MustParse("0")
	expQuota.ObjectMeta.Annotations["project-blocker.okd.cern.ch/old-pod-quota"] = "42"
	f.expectUpdateAction(expQuota)

	// namespace should be annotated
	expNs := ns.DeepCopy()
	expNs.ObjectMeta.Annotations["project-blocker.okd.cern.ch/status"] = "Project blocking completed"
	f.expectUpdateAction(expNs)

	// deploymentConfig should be scaled to zero and annotation should be added
	// NOTE: oc client apps action needs to be placed after kubectl actions
	expDc := dc.DeepCopy()
	expDc.Spec.Replicas = 0
	expDc.Annotations["project-blocker.okd.cern.ch/old-pod-replica-number"] = "1"
	f.expectUpdateAction(expDc)

	// run the controller to see if it works
	f.run(getKey(ns, t))
}


func TestBlockNamespaceSuspendCronjob(t *testing.T) {
	f := newFixture(t)
	ns := newNamespace(t.Name())
	ns.Labels["okd.cern.ch/project-blocked"] = "true"
	cronjob := newCronjob("test-cj", ns.Name)
	cronjob.Annotations["random-annotation"] = "lorem-ipsum"
	quota := newQuota(ns.Name, ns.Name)
	quota.Annotations["another-annotation"] = "lorem-ipsum"

	// add the pre-created object into our fake environment
	f.namespaceLister = append(f.namespaceLister, ns)
	f.kubeobjects = append(f.kubeobjects, ns, cronjob, quota)

	// cronjob should be suspended and annotation should be added
	expCronjob := cronjob.DeepCopy()
	expCronjob.Spec.Suspend = boolPtr(true)
	expCronjob.Annotations["project-blocker.okd.cern.ch/old-suspend-status"] = "false"
	f.expectUpdateAction(expCronjob)

	// quota should be set to zero and annotation should be added
	expQuota := quota.DeepCopy()
	expQuota.Spec.Hard[corev1.ResourcePods] = resource.MustParse("0")
	expQuota.ObjectMeta.Annotations["project-blocker.okd.cern.ch/old-pod-quota"] = "42"
	f.expectUpdateAction(expQuota)

	// namespace should be annotated
	expNs := ns.DeepCopy()
	expNs.ObjectMeta.Annotations["project-blocker.okd.cern.ch/status"] = "Project blocking completed"
	f.expectUpdateAction(expNs)

	// for _, a := range f.kubeactions {
	// 	fmt.Printf("action: %v\n", a)
	// }

	// run the controller to see if it works
	f.run(getKey(ns, t))
}


func TestBlockNamespaceDeletePod(t *testing.T) {
	f := newFixture(t)
	ns := newNamespace(t.Name())
	ns.Labels["okd.cern.ch/project-blocked"] = "true"
	quota := newQuota(ns.Name, ns.Name)
	alonePod := newPod("test-pod-alone", ns.Name)

	// add the pre-created object into our fake environment
	f.namespaceLister = append(f.namespaceLister, ns)
	f.kubeobjects = append(f.kubeobjects, ns, quota, alonePod)

	// quota should be set to zero and annotation should be added
	expQuota := quota.DeepCopy()
	expQuota.Spec.Hard[corev1.ResourcePods] = resource.MustParse("0")
	expQuota.Annotations["project-blocker.okd.cern.ch/old-pod-quota"] = "42"
	f.expectUpdateAction(expQuota)

	// the standalone pod should be deleted, the other one should be left untouched
	expPod := alonePod.DeepCopy()
	f.expectDeleteAction(expPod)

	// namespace should be annotated
	expNs := ns.DeepCopy()
	expNs.ObjectMeta.Annotations["project-blocker.okd.cern.ch/status"] = "Project blocking completed"
	f.expectUpdateAction(expNs)

	// run the controller to see if it works
	f.run(getKey(ns, t))
}

func TestUnblockNamespaceScaleDeployment(t *testing.T) {
	f := newFixture(t)
	// a project that is currently blocked and should be unblocked
	ns := newNamespace(t.Name())
	ns.Annotations["project-blocker.okd.cern.ch/status"] = "Project blocking completed"
	ns.Labels["okd.cern.ch/project-blocked"] = "false"

	// quota of the (currently) blocked namespace
	quota := newQuota(ns.Name, ns.Name)
	quota.Spec.Hard[corev1.ResourcePods] = resource.MustParse("0")
	quota.Annotations["project-blocker.okd.cern.ch/old-pod-quota"] = "42"
	// adding an extra annotation here to test if the controller is overwriting the entire
	// annotation map - it should not!
	quota.Annotations["random-annotation"] = "lorem-ipsum"

	// a deployment in the blocked project that was scaled by controller
	deploy1 := newDeployment("test-deploy-1", ns.Name, int32Ptr(0))
	deploy1.Annotations["project-blocker.okd.cern.ch/old-pod-replica-number"] = "3"

	// a deployment in the blocked project that was not scaled by the controller
	// this deployment should be left untouched
	deploy2 := newDeployment("test-deploy-2", ns.Name, int32Ptr(0))

	// add the pre-created object into our fake environment
	f.namespaceLister = append(f.namespaceLister, ns)
	f.kubeobjects = append(f.kubeobjects, ns, quota, deploy1, deploy2)

	// quota should be set to 42 and annotation should be removed
	expQuota := quota.DeepCopy()
	expQuota.Spec.Hard[corev1.ResourcePods] = resource.MustParse("42")
	expQuota.SetAnnotations(map[string]string{"random-annotation": "lorem-ipsum"})
	f.expectUpdateAction(expQuota)

	// deployment 1 should be scaled up
	expDeploy := deploy1.DeepCopy()
	expDeploy.Spec.Replicas = int32Ptr(3)
	expDeploy.Annotations = map[string]string{}
	f.expectUpdateAction(expDeploy)

	// namespace should be annotated
	expNs := ns.DeepCopy()
	expNs.ObjectMeta.Annotations["project-blocker.okd.cern.ch/status"] = "Project unblocking completed"
	f.expectUpdateAction(expNs)

	// run the controller to see if it works
	f.run(getKey(ns, t))
}

func TestUnblockNamespaceMalformedAnnotation(t *testing.T) {
	f := newFixture(t)
	// a project that should be unblocked
	ns := newNamespace(t.Name())
	ns.Annotations["project-blocker.okd.cern.ch/status"] = "Project blocking completed"
	ns.Labels["okd.cern.ch/project-blocked"] = "false"

	// quota of the (currently) blocked namespace
	quota := newQuota(ns.Name, ns.Name)
	quota.Spec.Hard[corev1.ResourcePods] = resource.MustParse("0")
	// a faulty annotation
	quota.SetAnnotations(map[string]string{"project-blocker.okd.cern.ch/old-pod-quota": ""})

	// add the pre-created object into our fake environment
	f.namespaceLister = append(f.namespaceLister, ns)
	f.kubeobjects = append(f.kubeobjects, ns, quota)

	// run the controller to see if it works
	f.runExpectError(getKey(ns, t))
}
