// This is a generated file. Do not edit directly.

module gitlab.cern.ch/paas-infra/webservices-blocker-controller

go 1.16

require (
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/openshift/api v0.0.0-20210910062324-a41d3573a3ba
	github.com/openshift/client-go v0.0.0-20210521082421-73d9475a9142
	github.com/spf13/pflag v1.0.5
	k8s.io/api v0.21.1
	k8s.io/apimachinery v0.21.1
	k8s.io/client-go v0.21.1
	k8s.io/klog/v2 v2.40.1
)

replace k8s.io/client-go/kubernetes => github.com/openshift/kubernetes-client-go v0.0.0-20210521075216-71b63307b5df
