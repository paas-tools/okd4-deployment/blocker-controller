{{/*
Expand the name of the chart.
*/}}
{{- define "webservices-blocker-controller.name" -}}
{{- default .Release.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "webservices-blocker-controller.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "webservices-blocker-controller.labels" -}}
helm.sh/chart: {{ include "webservices-blocker-controller.chart" . }}
{{ include "webservices-blocker-controller.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "webservices-blocker-controller.selectorLabels" -}}
app.kubernetes.io/name: {{ include "webservices-blocker-controller.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "webservices-blocker-controller.serviceAccountName" -}}
{{- include "webservices-blocker-controller.name" . }}
{{- end }}
