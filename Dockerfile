# Build the manager binary
FROM docker.io/golang:1.17 as builder

WORKDIR /workspace
COPY . .

# Build
RUN CGO_ENABLED=0 go build -ldflags '-s -w' -a -o controller .

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot
WORKDIR /
COPY --from=builder /workspace/controller .
USER nonroot:nonroot

ENTRYPOINT ["/controller"]
